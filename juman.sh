# jumanpp-2.0.0-rc2 download
wget https://github.com/ku-nlp/jumanpp/releases/download/v2.0.0-rc2/jumanpp-2.0.0-rc2.tar.xz
# unzip a file
tar xvf jumanpp-2.0.0-rc2.tar.xz
# cmake install
apt install cmake -y
# build jumanpp
cd jumanpp-2.0.0-rc2/
mkdir build
cd build/
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local
make
# install jumanpp
make install