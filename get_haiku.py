from urllib import request
import time
import re
import pickle

re_haiku = re.compile('<div align="center" class="title"><B>.*?</B></div>')
re_author = re.compile('<td background="img/bg6.gif"><div align="center" class="fontff"><font color="#FFFFFF">作　者</font></div></td>\s*<td background="img/bg-w.gif" bgcolor="#FFFFFF"><div align="center" class="font2">.*?</div></td>')
re_kigo = re.compile('<td width="24%" background="img/bg6.gif"><div align="center" class="fontff"><font color="#FFFFFF">季　語</font></div></td>\s*<td width="76%" background="img/bg-w.gif" bgcolor="#FFFFFF"> <div align="center" class="font2">.*?</div></td>')
re_season = re.compile('<td background="img/bg6.gif"><div align="center"><span class="fontff"><font color="#FFFFFF">季　節</font></span></div></td>\s*<td background="img/bg-w.gif" bgcolor="#FFFFFF"><div align="center" class="font2">.*?</div></td>')

def extract(page):
    title = re_haiku.findall(page)
    author = re_author.findall(page)
    kigo = re_kigo.findall(page)
    season = re_season.findall(page)
    if False in [len(i) == 1 for i in [title, author, kigo, season]]:
        print(title,author,kigo,season)
        return ['','','','']
    return [title[0][37:-15], author[0][210:-16], kigo[0][235:-16], season[0][223:-16]]

url = 'http://www.haiku-data.jp/work_detail.php?cd='

haiku_data = []

for i in range(1,42000):
    while True:
        time.sleep(1)
        try:
            page = request.urlopen(url + str(i), timeout=10)
        except:
            print(i)
            continue
        break
    haiku = extract(page.read().decode('utf-8'))
    print(i,haiku)
    haiku_data.append(haiku)

with open('DATA/haiku.pickle', 'wb') as f:
    pickle.dump(haiku_data, f)
