import pickle
import re
import numpy as np

pattern = re.compile('[(（].*?[)）]')

# 俳句データを読み込む
with open('DATA/haiku.pickle', 'rb') as f:
    raw_haiku = pickle.load(f)

haiku_list = []
for haiku, *_ in raw_haiku:
    # 空の俳句は切り捨てる
    if haiku == '':
        continue

    # 各記号を取り除く
    haiku = haiku.replace('\u3000', ' ').replace('/', ' ').replace('／', ' ')

    # カッコで囲まれた部分を取り除く
    for part in  pattern.findall(haiku):
        haiku = haiku.replace(part, ' ')
    
    haiku_list.append(haiku)

# 俳句をシャッフルする
np.random.shuffle(haiku_list)

# 俳句を学習データとバリデーションデータに分ける
valid_num = len(haiku_list)//10

with open('DATA/raw_valid.txt', 'w') as f:
    for haiku in haiku_list[:valid_num]:
        f.write(haiku + '\n')

with open('DATA/raw_train.txt', 'w') as f:
    for haiku in haiku_list[valid_num:]:
        f.write(haiku + '\n')
