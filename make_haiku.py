#! /usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
import random
import logging
import random
import datetime

import torch
import torch.nn.functional as F
from torch.nn.modules.loss import CrossEntropyLoss
import numpy as np
from tqdm import tqdm, trange
from transformers.tokenization_bert import BertTokenizer
from transformers.optimization import AdamW, WarmupLinearSchedule
from transformers import CONFIG_NAME, WEIGHTS_NAME
from torch import optim
from torch.utils.tensorboard import SummaryWriter

from model import BertMouth
from data import make_dataloader

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(name)s -   %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def parse_argument():
    parser = argparse.ArgumentParser()

    parser.add_argument("--bert_model", default=None, type=str, required=True,
                        help="Bert pre-trained model selected in the list: bert-base-uncased, "
                        "bert-large-uncased, bert-base-cased, bert-large-cased, bert-base-multilingual-uncased, "
                        "bert-base-multilingual-cased, bert-base-chinese.")
    parser.add_argument("--output_dir", default="./models", type=str,
                        help="The output directory where the model checkpoints and predictions will be written.")
    parser.add_argument("--train_file", default=None,
                        type=str, help="A file path for training.")
    parser.add_argument("--valid_file", default=None,
                        type=str, help="A file path for validation.")
    parser.add_argument("--max_seq_length", default=128, type=int,
                        help="The maximum total input sequence length after WordPiece tokenization. Sequences "
                             "longer than this will be truncated, and sequences shorter than this will be padded.")
    parser.add_argument("--do_train", action='store_true',
                        help="Whether to run training.")
    parser.add_argument("--do_generate", action='store_true',
                        help="Whether to generate text.")
    parser.add_argument("--train_batch_size", default=32,
                        type=int, help="Total batch size for training.")
    parser.add_argument("--learning_rate", default=5e-5,
                        type=float, help="The initial learning rate for Adam.")
    parser.add_argument("--num_train_epochs", default=3.0, type=float,
                        help="Total number of training epochs to perform.")
    parser.add_argument("--no_cuda",
                        action='store_true',
                        help="Whether not to use CUDA when available.")
    parser.add_argument('--seed',
                        type=int,
                        default=-1,
                        help="A random seed for initialization.")
    parser.add_argument('--max_iter',
                        type=int,
                        default=10,
                        help="The number of iterations in text generation.")
    parser.add_argument('--seq_length',
                        type=int,
                        default=50,
                        help="The sequence length generated.")
    parser.add_argument("--adam_epsilon", default=1e-8, type=float,
                        help="Epsilon for Adam optimizer.")
    parser.add_argument("--fix_word", default=None, type=str,
                        help="A fixed word in text generation.")
    parser.add_argument("--samples", default=10, type=int,
                        help="The number of generated texts.")

    args = parser.parse_args()
    return args


def initialization_text(tokenizer, length, fix_word):
    except_tokens = ["[MASK]", "[PAD]", "[UNK]", "[CLS]", "[SEP]"]
    except_ids = [tokenizer.vocab[token] for token in except_tokens]
    candidate_ids = [i for i in range(tokenizer.vocab_size)
                     if i not in except_ids]

    init_tokens = []
    init_tokens.append(tokenizer.vocab["[CLS]"])
    for _ in range(length):
        init_tokens.append(random.choice(candidate_ids))
    init_tokens.append(tokenizer.vocab["[SEP]"])

    return init_tokens


def generate(tokenizer, device, max_iter=10, length=50, max_length=128,
             model=None, fix_word=None, samples=1):
    if isinstance(model, str):
        model_state_dict = torch.load(os.path.join(model, "pytorch_model.bin"),
                                      map_location=device)
        model = BertMouth.from_pretrained(model,
                                          state_dict=model_state_dict,
                                          num_labels=tokenizer.vocab_size)
        model.to(device)

    except_tokens = ["[MASK]", "[PAD]", "[UNK]", "[CLS]", "[SEP]"]
    except_ids = [tokenizer.vocab[token] for token in except_tokens]

    results = []
    for iteration in range(samples):
        generated_token_ids = initialization_text(tokenizer, length, fix_word)

        if fix_word:
            tokenized_fix_word = tokenizer.tokenize(fix_word)
            fix_word_pos = random.randint(1,
                                          length - len(tokenized_fix_word))
            fix_word_interval = set(range(fix_word_pos,
                                          fix_word_pos + len(tokenized_fix_word)))
            for i in range(len(tokenized_fix_word)):
                generated_token_ids[fix_word_pos + i] = \
                    tokenizer.convert_tokens_to_ids(tokenized_fix_word[i])

        else:
            fix_word_interval = []

        input_type_id = [0] * max_length
        input_mask = [1] * len(generated_token_ids)
        while len(input_mask) < max_length:
            generated_token_ids.append(0)
            input_mask.append(0)

        generated_token_ids = torch.tensor([generated_token_ids],
                                           dtype=torch.long).to(device)
        input_type_id = torch.tensor(
            [input_type_id], dtype=torch.long).to(device)
        input_mask = torch.tensor([input_mask], dtype=torch.long).to(device)

        pre_tokens = generated_token_ids.clone()
        for _ in range(max_iter):
            for j in range(length):
                if fix_word_interval:
                    if j + 1 in fix_word_interval:
                        continue

                generated_token_ids[0, j + 1] = tokenizer.vocab["[MASK]"]
                logits = model(generated_token_ids,
                               input_type_id, input_mask)[0]
                sampled_token_id = torch.argmax(logits[j + 1])
                if sampled_token_id not in except_ids:
                    generated_token_ids[0, j + 1] = sampled_token_id
                else:
                    generated_token_ids[0, j + 1] = pre_tokens[0][j + 1]
            sampled_sequence = [tokenizer.ids_to_tokens[token_id]
                                for token_id in generated_token_ids[0].cpu().numpy()]
            sampled_sequence = " ".join([token[2:] if token.startswith("##") else token
                                        for token in sampled_sequence[1:length + 1]])
            if torch.equal(pre_tokens, generated_token_ids):
                break
            pre_tokens = generated_token_ids.clone()
        logger.info("{0} sampled sequence{1}/{2}: {3}".format(length, iteration, samples, sampled_sequence))
        results.append(sampled_sequence)
    return results

def main():
    args = parse_argument()

    if args.seed is not -1:
        random.seed(args.seed)
        np.random.seed(args.seed)
        torch.manual_seed(args.seed)

    device = torch.device("cuda" if torch.cuda.is_available()
                          and not args.no_cuda else "cpu")

    if device != "cpu":
        torch.cuda.manual_seed_all(args.seed)

    tokenizer = BertTokenizer.from_pretrained(args.bert_model, do_lower_case=False,
                                              tokenize_chinese_chars=False)

    for length in range(5, 20):
        haiku_list = generate(tokenizer, device, max_iter=args.max_iter,
                 length=length, model=args.bert_model,
                 fix_word=args.fix_word, samples=args.samples)
        with open('result/haiku_{0:02d}.txt'.format(length), 'w') as f:
            f.write('\n'.join(haiku_list)+'\n')


if __name__ == '__main__':
    main()
