# haiku generate with BertMouth

## Requirement
- Python3
- juman++

## usage
```
./juman.sh
wget http://nlp.ist.i.kyoto-u.ac.jp/nl-resource/JapaneseBertPretrainedModel/Japanese_L-12_H-768_A-12_E-30_BPE_transformers.zip
unzip Japanese_L-12_H-768_A-12_E-30_BPE_transformaers.zip
pip install -r requirements.txt

python get_haiku.py
python make_data.py

python preprocess.py \
  --input_file DATA/raw_train.txt \
  --output_file DATA/train.txt \
  --model ./jumanpp-2.0.0-rc2/model/jumandic.jppmdl
python preprocess.py \
  --input_file DATA/raw_valid.txt \
  --output_file DATA/valid.txt \
  --model ./jumanpp-2.0.0-rc2/model/jumandic.jppmdl
python ./bert_mouth.py \
  --bert_model ./Japanese_L-12_H-768_A-12_E-30_BPE_transformers/ \
  --output_dir ./model \
  --train_file DATA/train.txt \
  --valid_file DATA/valid.txt \
  --max_seq_length 128 \
  --do_train \
  --train_batch_size 10 \
  --num_train_epochs 100
python ./bert_mouth.py \
  --bert_model ./model \
  --do_generate \
  --seq_length 11 \
  --max_iter 20
```
